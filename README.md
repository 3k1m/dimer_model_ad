## BACKGROUND

This code accompanies the manuscript "From reaction kinetics to dementia: a simple dimer model of Alzheimer's disease etiology" by Michael R. Lindstrom, Manuel B. Chavez, Elijah A. Gross-Sable, Eric Y. Hayden, and David B. Teplow.

## GETTING STARTED

The code is written for Python 3. The code will reproduce the graphs and values found in the paper.