#!/usr/bin/env python
# coding: utf-8

"""This code accompanies the paper 'From reaction kinetics to dementia: a simple dimer model of Alzheimer's disease etiology'
by Michael R. Lindstrom, Manuel B. Chavez, Elijah A. Gross-Sable, Eric Y. Hayden, and David B. Teplow
"""

# In[1]:


import math
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as op
import scipy.special as spec


# In[2]:


# to have decent labels in the plots/graphs to come...
pltfont = {'family' : 'normal',
        'weight' : 'bold',
           'size' : 14 }

plt.rc('font', **pltfont)


# In[3]:


"""This first part establishes a few constants for monomer production and clearance, and the 
times over which these rates vary during the aging process. We also establish the dimer scale and
diffusivity values, the empirical production/clearance functions that vary with age, and the monomer scale."""

S42 = 330e-15 # M/s production rate for Ab42
ratio42_to_40 = 0.1 # Ab42 to Ab40 ratio

Sbar = S42*(1+1/ratio42_to_40) # Abeta production rate, M/s
SDbar = 1.5*Sbar # because 50% more APP in Down Syndrome

S_increase_years = 100 # took 100 years to go up
S_increase_fraction = 0.65

S_increase_years_DS = 50 # for Down Syndrome, took 50 years to go up
S_increase_fraction_DS = 0.58 

kappa30 = 4.54e-5 # /s, estimated from the Ab42 FTR (in units of /hour are 0.1636 and 0.06667)
kappa80 = 1.85e-5 # /s
kappa_decrease_years = 50

Dbar = 1.e-12  # M, dimer scale

DD = 4.3e-7 # cm^2/s, dimer diffusivity
DM = 5.47e-7 # cm^2/s, monomer diffusivity

# useful constant for seconds in a year
year = 365*24*3600

# we model S(t) = Sbar(1+t/tau_s)    
# S goes up by 65% from 0 to 100y
# ==> 100y/tau_s = 0.65
# ==> tau_s = 100y/0.65
tau_s = S_increase_years*year/S_increase_fraction # s
tau_sd = S_increase_years_DS*year/S_increase_fraction_DS # s, tau but for Down Syndrome case

S_tdep = lambda t: Sbar*(1+t/tau_s) # production rate as time dependent, t in seconds
    
# we model kappa(t) = kappabar(1-t/tau_k)
# kappa decreases from 5.07e-5 to 2.05e-5 from age 30 to 80
# first find slope
kappa_slope = (kappa80 - kappa30)/(kappa_decrease_years*year)
# so now we know kappa scale...
kappabar = kappa30 - 30*year*kappa_slope
# and to find tau_k: factor out kappabar from slope and take recipricol -- and negate since equation has -t/tau_k in it
tau_k = -kappabar/kappa_slope # s

kappa_tdep = lambda t: kappabar*(1-t/tau_k) # clearance rate as time dependent, t in seconds

# scales we approximate from our many assumptions
Mbar = Sbar/kappabar # M, scale for monomers

nu_over_mu = Dbar/(Mbar**2)

mubar = 0.4 # /s, from ~ geometric mean of two sets of data
nubar = nu_over_mu*mubar


# In[4]:


"""Here we use the Lambert and Cizas data to estimate the toxicity parameter sigma"""

tday = 24*3600 # seconds in a day

# here we use two papers

# start with Lambert
lam_concentrations = np.array([0, 5e-9, 50e-9, 500e-9, 5000e-9])
# estimates for how many died
lam_dead = np.array([0.1-11/48*0.1, 0.2+2/48*0.1,0.2+20/48*0.1,0.25+3/48*0.1,0.30+13/48*0.1])
# so this is how many survived
lam_survive = 1 - lam_dead
# estimate of errors: UNKNOWN IF SE, SD, 95% CI, ETC.!!!
lam_errs = np.array([16/2*0.1/48, 59/2*0.1/48, 56/2*0.1/48, 49/2*0.1/48, 78/2*0.1/48])

# we normalize by the survival in the Lambert control
# recall d(a/b) ~ sqrt( (da/b)**2 + (-a/b^2 db)**2 ) if we add standard errors in quadrature
lam_errs = np.sqrt( (lam_errs/lam_survive[0])**2 + ((lam_survive*lam_errs[0])/(lam_survive[0]**2))**2  )
# renormalize the survival
lam_survive /= np.max(lam_survive)

# now for Cizas paper
cizas_concentrations = np.array([0, 0.25e-6, 0.5e-6, 1.e-6, 2e-6])
cizas_survive = np.array([1-0.2*21/61, 1-0.2*21/61, 0.8-0.2*3/61, 0.2+0.2*17/61, 0.2-0.2*27/61])
# these are STANDARD ERRORS!
cizas_errs = np.array([0.2*12/61, 0.2*18/61, 0.2*24/61, 0.2*55/(2*61), 0.2*60/(2*61)])

# again we normalize by the control and use the quadrature as with Lambert
cizas_errs = np.sqrt( (cizas_errs/cizas_survive[0])**2 + ((cizas_survive*cizas_errs[0])/(cizas_survive[0]**2))**2  )
cizas_survive /= np.max(cizas_survive)

# make a super big dataset by combining both experiments

# in the first two commented cases below, we can check what happens if only one of the datasets is used

#fit_concentrations = np.concatenate( (lam_concentrations), axis=None) #np.copy(lam_concentrations)
#fit_vitalities = np.concatenate( (lam_survive), axis=None) #np.copy(lam_survive)
#fit_errs = np.concatenate( (lam_errs), axis=None) #np.copy(lam_errs)

#fit_concentrations = np.concatenate( (cizas_concentrations), axis=None) #np.copy(lam_concentrations)
#fit_vitalities = np.concatenate( (cizas_survive), axis=None) #np.copy(lam_survive)
#fit_errs = np.concatenate( (cizas_errs), axis=None) #np.copy(lam_errs)

# this combines both 
fit_concentrations = np.concatenate( (lam_concentrations,  cizas_concentrations), axis=None) #np.copy(lam_concentrations)
fit_vitalities = np.concatenate( (lam_survive,   cizas_survive), axis=None) #np.copy(lam_survive)
fit_errs = np.concatenate( (lam_errs,  cizas_errs), axis=None) #np.copy(lam_errs)

# fit for V = exp(-sigma D t)
exp_decay = lambda sig, cs, t: np.exp(-sig*t*cs)

# want to minimize the negative log likelihood
# log L = sum of log( 1/sqrt(2*pi*error_i**2) * exp(-(y_i - prediction_i)**2/(2*error_i**2)))
neg_log_like = lambda sig: -np.sum( -0.5*np.log(2*np.pi) - np.log(fit_errs) - ( ((fit_vitalities - exp_decay(sig, fit_concentrations,tday))**2)/(2*fit_errs**2)) )

# use initial guess of 1
fit_sig = op.fmin(func = neg_log_like, x0=[1])

# numerically estimate the Hessian (just second derivative here since it is a scalar optimization)
# the standard error is 1/Hessian * 1/sqrt(number of observations)
hess = (neg_log_like(fit_sig+1.e-2) - 2*neg_log_like(fit_sig) + neg_log_like(fit_sig-1.e-2))/(1.e-4)
SE = 1/np.sqrt(hess)*1/np.sqrt(len(fit_vitalities))

# linear span for the concentrations

con_lin_span = np.linspace(np.min(fit_concentrations), np.max(fit_concentrations)) # from first to last

# plot the Lambert data
plt.errorbar(lam_concentrations,lam_survive,yerr=2*lam_errs,fmt='gx', markersize='12', label='Lambert')
# and the Cizas data
plt.errorbar(cizas_concentrations,cizas_survive,yerr=2*cizas_errs,fmt='c*', markersize='12',label='Cizas')
# and our fit
plt.plot(con_lin_span, exp_decay(fit_sig, con_lin_span, tday), 'k-', label='Fit')
#plt.xticks(rotation=90) # so the tick labels don't overlap

# formatting and labelling
plt.semilogx()
plt.xlabel('Oligomer Concentration [M]')
plt.ylabel('Viability')
plt.legend()
plt.savefig('Lambert_Cizas.png',dpi=300, bbox_inches = "tight")
plt.show()


# this is our death rate constant!
sigmabar = fit_sig[0] # /M /s
print("sigmabar: %10.10g /M/s" % (sigmabar))
print("standard error: %10.10g /M/s" % (SE))


# In[5]:


'''We print all the scales chosen'''

# some derived scales we choose
Tbar = 1/kappabar
Xbar = np.sqrt(DM*Tbar)

# print the scales - these should be in the paper
print("Dbar: %10.10g M" % (Dbar))
print("Mbar: %10.10g M" %(Mbar))
print("Sbar: %10.10g M" %(Sbar))
print("SDbar: %10.10g M" %(SDbar))
print("kappabar: %10.10g /s" %(kappabar))
print("nubar/mubar: %10.10g" %(nu_over_mu))
print("mubar: %10.10g /s" %(mubar))
print("nubar: %10.10g /(M s)" %(nubar))
print("LambdaSG: %10.10g s" %(tau_s))
print("LambdaSD: %10.10g s" %(tau_sd))
print("Lambdak: %10.10g s" %(tau_k))
print("Tbar: %10.10g s" %(Tbar))
print("Xbar: %10.10g cm" %(Xbar)) # in cm since diffusivity was in cm^2/s
print("sigmabar: %10.10g /(M s)" %(sigmabar))
print("Ubar: %10.10g /(M s)" %(sigmabar*Dbar))


# In[6]:


''' the PDE/ODE system we get is...
m_tau = lap m + stilde - kappatilde m - nu00(nutilde m^2 - mutilde d) - zeta00m zetatilde md
d_tau = xi0^2 lap d - mu00 (mutilde d - nutilde m^2) - zeta00d zetatild md
v_tau = -sigma00 sigmatilde d v
where
nu00 = Tbar nubar Mbar
zeta00m = Tbar zetabar Dbar
xi0 = sqrt(DD/DM)
mu00 = mubar Tbar
zeta00d = Tbar zetabar Mbar
'''
xi = np.sqrt(DD/DM)
nu00 = 2*Tbar*nubar*Mbar
zeta00m = Tbar*nubar*Dbar 
mu00 = mubar*Tbar
zeta00d = Tbar*nubar*Mbar 
sigma00 = sigmabar*Tbar*Dbar
lam00sg = tau_s/Tbar
lam00sd = tau_sd/Tbar
lam00k = tau_k/Tbar
epsilon = (1/lam00k/mu00)**(1./3) #1/lam00k #1/mu00 # (sigma00/mu00)**(1./3) #1/mu00 # this is a "small" parameter

# we want to find how big/small things are with respect to the small epsilon

def round_to_nearest(xin, nearest):
    """Given xin and a scale for where to round, rounds xin to a multiple of nearest"""
    return nearest*round(xin/nearest) # should round to the nearest

def log_b_x(b,x):
    """Just computes log_b x """
    return np.log(x)/np.log(b) 

deg_of_round = 1 # power of epsilon to round to

# these are the nearest integer powers of epsilon for each scale
nu0p = round_to_nearest(log_b_x(epsilon, nu00), deg_of_round)
zeta0mp = round_to_nearest(log_b_x(epsilon, zeta00m), deg_of_round)
xip = round_to_nearest(log_b_x(epsilon, xi), deg_of_round)
mu0p = round_to_nearest(log_b_x(epsilon, mu00), deg_of_round)
zeta0dp = round_to_nearest(log_b_x(epsilon, zeta00d), deg_of_round)
sigma0p = round_to_nearest(log_b_x(epsilon, sigma00), deg_of_round)
lam0sgp = round_to_nearest(log_b_x(epsilon, lam00sg), deg_of_round)
lam0sdp = round_to_nearest(log_b_x(epsilon, lam00sd), deg_of_round)
lam0kp = round_to_nearest(log_b_x(epsilon, lam00k), deg_of_round)

# pre-rounding just see the values
print("epsilon: %10.10g" %(epsilon))
print("nu00: %10.10g, %10.10g" %(nu00, nu0p ))
print("zeta00m: %10.10g, %10.10g" %(zeta00m, zeta0mp ))
print("xi: %10.10g, %10.10g" %(xi, xip ))
print("mu00: %10.10g, %10.10g" %(mu00, mu0p ))
print("zeta00d: %10.10g, %10.10g" %(zeta00d, zeta0dp ))
print("sigma00: %10.10g, %10.10g" %(sigma00, sigma0p ))
print("lambda00SG: %10.10g, %10.10g" %(lam00sg, lam0sgp ))
print("lambda00SD: %10.10g, %10.10g" %(lam00sd, lam0sdp ))
print("lambda00k: %10.10g, %10.10g" %(lam00k, lam0kp ))


# In[7]:


'''
The system is
m_tau = lap m + stilde - kappatilde m - nu0 (nutilde m^2 - mutilde d) - epsilon^2 zeta0m zetatilde m d
d_tau = xi^2 lap d + epsilon^{-1} mu0 (nutilde m^2 - mutilde d) - nu0/2 zetatilde m d
v' = -epsilon^2 sigma0 sigmatilde d v
with slowly varying rate parameters with a time scale of O(epsilon^{-2})
'''

# rescale to have O(1) constants that may be multipled by a power of epsilon
nu0 = nu00/(epsilon**nu0p)
zeta0m = zeta00m/(epsilon**zeta0mp)
xi0 = xi/(epsilon**xip)
mu0 = mu00/(epsilon**mu0p)
zeta0d = zeta00d/(epsilon**zeta0dp)
sigma0 = sigma00/(epsilon**sigma0p)
lam0SG = lam00sg/(epsilon**lam0sgp)
lam0SD = lam00sd/(epsilon**lam0sdp)
lam0k = lam00k/(epsilon**lam0kp)

# after absorbing epsilon factors
# these constants are better scaled
print("nu0: %10.10g" %(nu0))
print("zeta0m: %10.10g" %(zeta0m))
print("xi: %10.10g" %(xi0))
print("mu0: %10.10g" %(mu0))
print("zeta0d: %10.10g" %(zeta0d))
print("sigma0: %10.10g" %(sigma0))
print("lam0SG: %10.10g" %(lam0SG))
print("lam0SD: %10.10g" %(lam0SD))
print("lam0k: %10.10g" %(lam0k))


# In[8]:


# verifying definitions of scaled parameters with epsilon replaced by kappabar/mubar
print(2*nubar*Sbar/kappabar**2) # nu0
print(tau_k**(2/3) * nubar**2 * Sbar**2/(mubar**(1/3)*kappabar**3)) # zeta0
print(mubar**(2/3)/(kappabar*tau_k**(1/3))) # mu0
print(sigmabar*nubar*Sbar**2*tau_k**(2/3)/(mubar**(1/3)*kappabar**3)) # sigma0
print(tau_s*kappabar/(tau_k**(2/3)*mubar**(2/3))) #lam0SG
print(tau_sd*kappabar/(tau_k**(2/3)*mubar**(2/3))) # lam0SD 
print(tau_k**(1/3)*kappabar/(mubar**(2/3))) # lam0k


# In[9]:


"""Traumatic Brain Injury"""

# from literature
hits = np.array([0,1,2,3,4])
lows = np.array([1, 1.47, 2.02, 2.47, 2.72])
his = np.array([1, 1.54, 2.36, 3.51, 5.03])
relatives = (his+lows)/2
errs = (his-lows)/(2*1.96) # took estimats of their values and divided by 1.96 b/c I think they used 95% CI

# naive linear, our quadratic model function, and other candidates
lin = lambda m, x: 1 + m*x
quad = lambda a, x: (1+a*x)**2
efit = lambda a, x: np.exp(a*x)
newfit = lambda a, x: a[0] + (1-a[0])*(1+a[1]*x)**2

# returns a function to compute negative log likelihood
# the function returned takes one parameter, p
# fun = function to fit with, x = x-values, y = y-values, dy=standard errors
neg_log_L = lambda fun, x, y, dy: lambda p: -sum ( -0.5*np.log(math.pi) - np.log(dy) - 0.5*( (y - fun(p,x) )**2)/(dy**2) )

# fit for the m and a values, but don't include the point with zero error...
lin_m = op.fmin(func = neg_log_L(lin,hits[1:],relatives[1:], errs[1:]), x0 = [0.5])
quad_a = op.fmin(func = neg_log_L(quad,hits[1:],relatives[1:], errs[1:]), x0 = [0.5])
efit_a = op.fmin(func = neg_log_L(efit,hits[1:],relatives[1:], errs[1:]), x0 = [0.5])
new_fit_ab = op.fmin(func = neg_log_L(newfit,hits[1:],relatives[1:], errs[1:]), x0 = [0, 0.5])

# print those optimal values
print(lin_m)
print(quad_a)
print(efit_a)
print(new_fit_ab)

# range of values to print over
lin_rng = np.linspace(0,hits[-1])

# AIC values for the two models
AIC_lin = 2*(neg_log_L(lin,hits[1:],relatives[1:], errs[1:]))(lin_m) + 2
AIC_quad = 2*(neg_log_L(quad,hits[1:],relatives[1:], errs[1:]))(quad_a) + 2
AIC_efit = 2*(neg_log_L(efit,hits[1:],relatives[1:], errs[1:]))(efit_a) + 2
AIC_newfit = 2*(neg_log_L(efit,hits[1:],relatives[1:], errs[1:]))(efit_a) + 4

# print the respective AIC values
print(AIC_lin)
print(AIC_quad)
print(AIC_efit)
print(AIC_newfit)

# look at the results
plt.errorbar(hits,relatives,yerr=errs,fmt='g.',markersize='12', label='Clinical')
#plt.plot(lin_rng, lin(lin_m,lin_rng),'b-', markersize='12', label='Linear')
plt.plot(lin_rng, quad(quad_a,lin_rng), 'k.', markersize='12', label='Model')
plt.xlabel('Number of TBIs')
plt.ylabel('AD Relative Risk')
plt.legend()
plt.savefig('TBI.png',dpi=300, bbox_inches = "tight")
plt.show()


# In[10]:


"""Hippocampal Volume Stuff"""

# so we can do stuff in years
year_to_sec = lambda y: y*365*24*3600

# if all rates are constant: ideal aging
Lambda_base = lambda t: sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)*t
# viability
Hvol_base = lambda t: np.exp(-Lambda_base(t))
# derivative of the viability at time t
deriv_base = lambda t: -np.exp(-Lambda_base(t))*sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)

# now we have the age-dependent kappa and S

# want integral (1+t/tau_s)^2 / (1-t/tau_k)^2
anti_deriv = lambda t: (tau_k**2)/(tau_s**2) * ( (tau_s+tau_k)**2 / (tau_k - t) + 2 * (tau_s+tau_k) * np.log(tau_k - t) + t )    
# now have cumulative hazard
Lambda_tdep = lambda t: sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2) * (anti_deriv(t) - anti_deriv(0))   
# viability for time-dependent
Hvol_tdep = lambda t: np.exp(-Lambda_tdep(t))
# derivative of viability
deriv_tdep = lambda t: -np.exp(-Lambda_tdep(t))*sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)*(1+t/tau_s)**2*(1-t/tau_k)**(-2)

# ages we look at
age_range = np.linspace(0,100,40)

# returns a function of t that is the tangent line:
# point = point in question, f = function, fprime = its derivative
tan_line = lambda point, f, fprime: lambda t: f(point) + fprime(point)*(t-point)

# plot the curves: all rates constant + time-varying rates
plt.plot(age_range, Hvol_tdep(year_to_sec(age_range)), 'bv', markersize='10', label='Dynamic')

# now their tangents, only care about ages near 75 y/o
tangent_age_range = np.linspace(70,80)
age_centre = 75

# the relative rate of change per year, %/year
#Vp_to_V_base = deriv_base(year_to_sec(age_centre))/Hvol_base(year_to_sec(age_centre))*year*100
Vp_to_V_tdep = deriv_tdep(year_to_sec(age_centre))/Hvol_tdep(year_to_sec(age_centre))*year*100

# labels for tangent lines
tan_tdep_label = "V'(t)/V, {:+.2g}%/yr".format(Vp_to_V_tdep)

# this is how much we scale up the rates to match a proper AD pathology:
# we assume that the AD pathology has a certain relative rate of change of volume
# and we want to scale up the time-dependent (normal pathology) case accordingly
scale_AD = -1.06/Vp_to_V_tdep # how much to scale up AD pathology...

# see what the scaling is
print("AD rate rescale f: %10.10g" %(scale_AD))

# these are the hazard function and derivative of the viability functions and the viability function
Lambda_AD = lambda t: scale_AD*sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2) * (anti_deriv(t) - anti_deriv(0))
deriv_AD = lambda t: -np.exp(-Lambda_AD(t))*scale_AD*sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)*(1+t/tau_s)**2*(1-t/tau_k)**(-2)
# viability for AD case
Hvol_AD = lambda t: np.exp(-Lambda_AD(t))

# plot the curve for AD-pathology
plt.plot(age_range, Hvol_AD(year_to_sec(age_range)), 'b*', markersize='10', label='AD Pathology')

# compute the relative rate of change of volume %/year
Vp_to_V_AD = deriv_AD(year_to_sec(age_centre))/Hvol_AD(year_to_sec(age_centre))*year*100

# label for the curve
tan_AD_label = "V'(t)/V, {:+.2g}%/yr".format(Vp_to_V_AD)

# Gordon study data points for comparison
age1 = 63.4
age2 = 71.6

# want to know the hippocampal volume ratios age 75 to 65
health_base1 = Hvol_base(year_to_sec(age1))
health_base2 = Hvol_base(year_to_sec(age2))
health_dynamic1 = Hvol_tdep(year_to_sec(age1))
health_dynamic2 = Hvol_tdep(year_to_sec(age2))
health_AD1 = Hvol_AD(year_to_sec(age1))
health_AD2 = Hvol_AD(year_to_sec(age2))

vol_ratio_base = health_base2/health_base1
print("static ratio: %10.10g" %(vol_ratio_base))

vol_ratio_dynamic = health_dynamic2/health_dynamic1
print("dynamic ratio: %10.10g" %(vol_ratio_dynamic))

vol_ratio_AD = health_AD2/health_AD1
print("AD ratio: %10.10g" %(vol_ratio_AD))

vol_ratio_AD_to_CN = health_AD2/health_dynamic1
print("AD to CN ratio: %10.10g" % (vol_ratio_AD_to_CN))

print(tan_tdep_label)
print(tan_AD_label)

plt.xlabel('Age [yr]')
plt.ylabel('Fractional HV Remaining')
plt.legend()
plt.savefig('HVOL2.png',dpi=300, bbox_inches = "tight")
plt.show()


# In[11]:


"""Incidence and Prevalence Comparisons"""

T_D = 7.1 # the time to death from AD, in years
T_E = 78.5 # life expectancy

# prevalence data
prevalence_ages = np.array([62.5, 67.5, 72.5, 77.5, 82.5, 87.5])
prevalence_rates = np.array([0.008, 0.017, 0.033, 0.065, 0.128, 0.301])
prevalence_errs = np.array([0.001, 0.001, 0.003, 0.005, 0.005, 0.011])

t_end_max = 115

# incidence data
incidence_ages = np.array([60,65,70,75,80,85,90,95])
incidence_rates = np.array([0.0008, 0.0017, 0.0035, 0.0071, 0.0144, 0.0292, 0.0595, 0.1210])

# time-dependent incidence at 60 years old
I_tdep_60 = (lambda t: sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)*(1+t/tau_s)**2*(1-t/tau_k)**(-2))(60*year)

gamma = incidence_rates[0]/(I_tdep_60*year) # so as to match the data at 60y

# so we know what gamma is
print("gamma: %10.10g" %(gamma))

# survivorships t in sec
H_base = lambda t: Hvol_base(t)**gamma
H_tdep = lambda t: Hvol_tdep(t)**gamma

# incidences t in sec
I_base = lambda t: sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)*gamma + 0*t
I_tdep = lambda t: sigmabar*nubar*Sbar*Sbar/(mubar*kappabar**2)*(1+t/tau_s)**2*(1-t/tau_k)**(-2)*gamma

# prevalences: for simplicty, takes argument in years
P_base = lambda y: (H_base(year_to_sec(y-T_D)) - H_base(year_to_sec(y)))/H_base(year_to_sec(y-T_D))
P_tdep = lambda y: (H_tdep(year_to_sec(y-T_D)) - H_tdep(year_to_sec(y)))/H_tdep(year_to_sec(y-T_D))


# do fits for real data
prev_fit = np.polyfit( prevalence_ages, np.log(prevalence_rates), 1)
incid_fit = np.polyfit( incidence_ages, np.log(incidence_rates), 1)

double_time_prevalence = np.log(2)/prev_fit[0]
double_time_incidence = np.log(2)/incid_fit[0]

print("Real Prevalence Doubling Time: %10.10g yr" %(double_time_prevalence))
print("Real Incidence Doubling Time: %10.10g yr" %(double_time_incidence))

# set ranges to plot
prevalence_range = np.linspace(prevalence_ages[0], prevalence_ages[-1], 20)
incidence_range = np.linspace(incidence_ages[0], incidence_ages[-1], 20)

# do fits for model data
# note that prevalence function takes argument of years and returns prevalence
# incidence function takes argument in seconds and returns incidence per second, which needs to be converted to per year
prev_fit_model = np.polyfit( prevalence_range, np.log( P_tdep(prevalence_range)), 1)
incid_fit_model = np.polyfit( incidence_range, np.log( I_tdep(year_to_sec(incidence_range))*year ), 1)

double_time_prevalence_model = np.log(2)/prev_fit_model[0]
double_time_incidence_model = np.log(2)/incid_fit_model[0]

print("Model Prevalence Doubling Time: %10.10g yr" %(double_time_prevalence_model))
print("Model Incidence Doubling Time: %10.10g yr" %(double_time_incidence_model))

# returns a polynomial function of x from best fit values
poly = lambda p: lambda x: np.sum( [ p[len(p)-i-1]*x**i for i in range(len(p)) ], axis=0 )

# prevalence functions can accept argument in years
plt.plot(prevalence_ages, prevalence_rates, 'g*', markersize='12', label='Clinical')
plt.plot(prevalence_range, P_tdep(prevalence_range), 'k^', markersize='12', label='Dynamic')
plt.plot(prevalence_range, np.exp( ( poly( prev_fit  ) )(prevalence_range) ), 'g.' ) # label="clinical".format(double_time_prevalence))
plt.plot(prevalence_range, np.exp( ( poly( prev_fit_model ) )(prevalence_range) ), 'k-' )# label="dynamic".format(double_time_prevalence_model))
plt.plot(prevalence_range, P_base(prevalence_range), 'cv', markersize='12',label="Static")
plt.xlabel('Age [yr]')
plt.ylabel('Prevalence')
plt.semilogy()
plt.legend()
plt.savefig('Prevalence.png',dpi=300, bbox_inches = "tight")
plt.show()

# incidence functions accept argument in seconds
plt.plot(incidence_ages, incidence_rates, 'g*', markersize='12', label='Clinical')
plt.plot(incidence_range, I_tdep(year_to_sec(incidence_range))*year, 'k^', markersize='12', label='Dynamic')
plt.plot(incidence_range, np.exp( ( poly( incid_fit  ) )(incidence_range) ), 'g.' ) # label="clinical".format(double_time_incidence))
plt.plot(incidence_range, np.exp( ( poly( incid_fit_model ) )(incidence_range) ), 'k-' ) # label="dynamic".format(double_time_incidence_model))
plt.plot(incidence_range, I_base(year_to_sec(incidence_range))*year, 'cv', markersize='12',label="Static")
plt.xlabel('Age [yr]')
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.ylim(10**(-4.25),10**0.5)
plt.semilogy()
plt.legend()
plt.savefig('Incidence.png',dpi=300, bbox_inches = "tight")
plt.show()

# now look into lifetime risk
T_start = 60
LT_risk_static = 1 - np.exp(-gamma*( Lambda_base(year_to_sec(T_E)) - Lambda_base(year_to_sec(T_start))))
LT_risk_dynamic = 1 - np.exp(-gamma*( Lambda_tdep(year_to_sec(T_E)) - Lambda_tdep(year_to_sec(T_start))))
print("Lifetime Risk static: %10.10g" % (LT_risk_static))
print("Lifetime Risk dynammic: %10.10g" % (LT_risk_dynamic))

print("omegabar: %10.10g /(M s)" %(gamma*sigmabar*Dbar))


# In[12]:


"""Here we do the spatial analysis making use of the Green's function solution"""
# Green's function steady state
# z, tau, m, d = O(1)

max_age = 80*365*24*3600/Tbar # range of ages considered

# dimensionless length range we consider on z-scale
r_rng = np.linspace(0,6,25)

r_end_extra = 7.5 # extra space

# this is in dimensionless form
R = 2 # hypothetical radius of source
k = 1 # clearance rate
Ssize = quad_a # perturbation size

# m as function of r
def get_m(r):
    rk = np.sqrt(k)
    
    r_greater_R = lambda r: np.exp(-rk*r)/(rk*r)*(R*np.cosh(rk*R)/rk - np.sinh(rk*R)/k)
    
    r_less_R = lambda r: np.sinh(rk*r)/(rk*r)*(r*np.exp(-rk*r)/rk - R*np.exp(-rk*R)/rk - 1/k*np.exp(-rk*R) + 1/k*np.exp(-rk*r)) + r_greater_R(R)
   
    return 1 + Ssize * ( np.heaviside(r-R,0.5)*r_greater_R(r) + np.heaviside(R-r,0.5)*r_less_R(r) )
    
# d is m**2 with mu and nu both 1
def get_d(r):
    return get_m(r)**2

# viability is integral of d 
def get_v(r,t):
    return np.exp(-epsilon**2 * sigma0*t*get_d(r))

ts = np.linspace(0,max_age,4) # range of dimensionless tvalues

# convert dimensionless times to years
to_years = lambda dimlesst: dimlesst * Tbar / (365*24*3600)

plt.plot(Xbar*r_rng, get_m(r_rng), 'c^', label='Monomers'.format(Mbar))
plt.plot(Xbar*r_rng, get_d(r_rng), 'bv', label='Dimers'.format(Dbar))
plt.plot(Xbar*r_rng, 1 + Ssize * np.heaviside(R-r_rng,0.), 'r*', label='Production'.format(Sbar))
plt.axvline(x=R*Xbar, linewidth=2, color='k', linestyle='--') # to mark the source boundary

plt.xlabel("Distance r [cm]")
plt.ylabel('Dimensionless Units')
plt.legend()
plt.savefig('space_all_O1_monomer_dimer.png',dpi=300, bbox_inches = "tight")
plt.show()

syms = ["o", "s", "D", "P" ]

ell = 0 # line width
for t in ts:
    plt.plot(Xbar*r_rng, get_v(r_rng,t), marker=syms[ell], linestyle="None", color="g", label = '{:.2g} y'.format(to_years(t)))    
    ell = ell+1

plt.axvline(x=R*Xbar, linewidth=2, color='k', label='boundary', linestyle='--') # to mark the source boundary    
    
plt.xlim((0,r_end_extra*Xbar))
plt.legend(bbox_to_anchor=(1.5, 0.5), loc='center', ncol=1)
plt.ylabel("Viability")
plt.xlabel("Distance r [cm]")
plt.savefig('space_all_O1_viability.png',dpi=300, bbox_inches = "tight")
plt.show()


# In[13]:


'''We consider some random fluctuations, building a model
where U can take on two different values.'''
import random
import scipy.stats as stats

Ubar = (nubar*sigmabar*Sbar**2)/(mubar*kappabar**2)

Frat = np.sqrt(scale_AD-1) # estimate of stdev:mean

# all times we reference are in seconds
# unless the variable name mentions years

year_in_sec = 365*24*3600

pr_plot = 0.02 # only plot this fraction of curves

num_realizations = 400 # 250 # 1000 #1000 # run this many realizations
cohort_size = 40000 #25000 # 10000 # how many people in the population
num_steps = 250 # 90 # 180 # steps forward over age ranges

begin_age = 0
end_age = 90

# this is to model the binomial option
# U ~ s.t. Pr(U = Umin) = p, Pr(U = Umax) = 1-p
# with EU = Ubar, Var U = F^2 Ubar^2
Umin = Ubar/10 # fixed lower U-value

Uplus = ((Frat**2+1)*Ubar**2 - Ubar*Umin)/(Ubar-Umin)
pUmin = (Uplus-Ubar)/(Uplus-Umin)

print("Umin: %g" % Umin)
print("Uplus: %g" % Uplus)
print("p: %g" % pUmin)

EU_bin = pUmin*Umin + (1-pUmin)*Uplus
VU_bin = pUmin*Umin**2 + (1-pUmin)*Uplus**2 - EU_bin**2

print("testing distribution properties")
print(EU_bin)
print(Ubar)
print(VU_bin)
print(Frat**2*Ubar**2)

# binomial distribution here will return Umin or Uplus
def bin_dist():
    if random.uniform(0,1) < pUmin:
        return Umin
    else:
        return Uplus

years, dT_year_sim = np.linspace(start=begin_age, stop=end_age, num=num_steps, retstep=True)
years_sub = years[:-1] # all but the last year, say

ages = years 
num_ages = len(ages)

# this function returns the index of an array arr whose corresponding value
# is nearest val
def find_nearest(arr, val): # to find the closest index to a given value
    return (np.abs(arr - val)).argmin()    

# useful setup for where we extract indices from our data
ages_wanted = np.array([60, 65, 70, 75, 80, 85, 90])
indices_wanted = [find_nearest(years, a) for a in ages_wanted] # indices of important years in ages_wanted

# a logical array so we can index logically
logicals = np.zeros(len(ages), dtype=bool)
for i in indices_wanted:
    logicals[i] = True

# the Person class represents an individual
# they start healthy but can develop AD
# they start alive but they die if they have AD for too long
class Person:
    def __init__(self, U0, Ufun):
        self.healthy = True # start healthy
        self.alive = True # alive
        self.age_diagnosed = None # not diagnosed
        self.u0 = U0 # initial Ubar
        self.uscale = Ufun # their scaling function
    def getAD(self,Tnow,dTyear):        
        # this r ~ Exp(Uvalue*gamma)
        r = -1/(gamma*self.getUnow(Tnow) )*np.log( 1 - random.uniform(0,1) )
        
        if( r < dTyear*year_in_sec ): # unlucky enough to get AD this time step
            self.healthy = False
            self.age_diagnosed = Tnow
    
    def die(self, Tnow):
        if not self.healthy: # so they had AD by this time
            if self.age_diagnosed + T_D*year_in_sec < Tnow*year_in_sec: # have had AD more than death-time
                self.alive = False
                
    def getUnow(self, Tnow):
        return self.u0*self.uscale(Tnow)
    
    def diagnosed_in(self,Tlow, Thi):
        if self.age_diagnosed is None: # so they were not diagnosed yet
            return False
        else:
            return Tlow < self.age_diagnosed and self.age_diagnosed <= Thi
        
    def diagnosed_by(self,Tnow):
        if self.age_diagnosed is None:
            return False
        else:
            return self.age_diagnosed <= Tnow    

# determines if there is a valuve within val_list that is "equal" to val
# use this because comparing floating points for equality is dangerous
delta = 2.5e-1 

def float_in(val, val_list):
    for v in val_list:
        if np.abs(val-v) < delta: # so within machine error-ish           
            return True, v    
    return False, None
        
# function to calculate the prevalence of AD given
# a list of people
def get_P(Person_list):
    alive = 0
    with_AD = 0   
    for p in Person_list: # look at everyone
        if p.alive: # increase alive count
            alive = alive+1
            if not p.healthy: # increase with AD count
                with_AD = with_AD+1
    return with_AD/alive

# function to calculate the incidence of AD given
# a list of people, a current time, and a time step
def get_I(Person_list, Tnow, dTyear):
    # if they were healhty at Tnow-dT => not diagnosed by that time
    Hold = np.sum([1 for p in Person_list if not p.diagnosed_by(Tnow - dTyear*year_in_sec)])
    # new number of healthy people
    Hnew = np.sum([1 for p in Person_list if not p.diagnosed_by(Tnow)])
    
    # relative change divided by dT
    return -(Hnew-Hold)/(Hold*dTyear)

# scaling is governed by S^2/kappa^2
def rescale(t):
    return (S_tdep(t)/kappa_tdep(t))**2*(kappa_tdep(0)/S_tdep(0))**2


# In[14]:


# we will store the incidence and prevalence at various ages for each realization

# when U is is constant and no distribution
incidences_Uconst = np.zeros( (num_realizations, num_ages))
prevalences_Uconst = np.zeros( (num_realizations, num_ages))

u_ad_Uconst = np.zeros( (num_realizations, num_ages))
u_nad_Uconst = np.zeros( (num_realizations, num_ages))

# we first do a simple case where Ubar is just one value    

for num in range(num_realizations): # per iteration
    
    print("num %d" % num)
    
    # their U function always returns the constant Ubar
    cohort = [Person( Ubar, lambda t: 1 ) for i in range(cohort_size)] # this many people in the cohort are healthy
    prev = np.array([0]) # prevalences
    incid = [ get_I(cohort, 0, dT_year_sim) ]
    u_ad = np.mean( [p.getUnow(0) for p in cohort if (p.healthy and p.alive)])
    u_nad = np.mean( [p.getUnow(0) for p in cohort if ( (not p.healthy) and p.alive)] )
    
    prev_by_age = [0] # for each special age will store the stats for this simulation
    inc_by_age = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if (p.healthy and p.alive)]) ]
    u_nad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] ) ]
    
   
    for year in years_sub: # start at age t=0, go up to age year_end
        # look at each person
        for p in cohort:
            p.die(year) # remove those who die of AD
            
            if p.healthy: # need to simulate
                p.getAD(year*year_in_sec,dT_year_sim)        
        
        P_now = get_P(cohort)
        I_now = get_I(cohort, year*year_in_sec, dT_year_sim)
        
        u_ad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if (p.healthy and p.alive)])
        u_nad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] )
      
        
        prev = np.append(prev,P_now)
        incid = np.append(incid,I_now)
        u_ad = np.append(u_ad, u_ad_now)
        u_nad = np.append(u_nad, u_nad_now)
        
        res, yr = float_in(year, ages)       
        
        if res: # so a year to document            
            prev_by_age.append(P_now)
            inc_by_age.append(I_now)
            u_ad_by_age.append(u_ad_now)
            u_nad_by_age.append(u_nad_now)
            #years_Uconst.append(yr)     
    
    if random.uniform(0,1) < pr_plot:
        plt.subplot(1,2,1)
        plt.plot(years, 100*prev)
        plt.subplot(1,2,2)
        plt.plot(years, incid)
    
    incidences_Uconst[num,:] = np.array([inc_by_age])
    prevalences_Uconst[num,:] = np.array([prev_by_age])
    u_ad_Uconst[num,:] = np.array([u_ad_by_age])
    u_nad_Uconst[num,:] = np.array([u_nad_by_age])

plt.subplot(1,2,1)
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.subplot(1,2,2)
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Uconst_many.png',dpi=300, bbox_inches = "tight")
plt.tight_layout()
plt.show()


# In[15]:


"""From the simulations, obtain statistics such as median values, 95% confidence window, etc."""

prevalences_Uconst_med = np.median(prevalences_Uconst, axis=0) # median and percentiles year by year
prevalences_Uconst_2p5 = np.percentile(prevalences_Uconst, 2.5, axis=0)
prevalences_Uconst_97p5 = np.percentile(prevalences_Uconst, 97.5, axis=0)
prevalences_Uconst_mean = np.mean(prevalences_Uconst, axis=0)

plt.plot(ages, 100*prevalences_Uconst_mean)
plt.plot(ages, 100*prevalences_Uconst_2p5, '--')
plt.plot(ages, 100*prevalences_Uconst_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.savefig('Uconst_prev.png',dpi=300, bbox_inches = "tight")
plt.show()

incidences_Uconst_med = np.median(incidences_Uconst, axis=0) # median and percentiles year by year
incidences_Uconst_2p5 = np.percentile(incidences_Uconst, 2.5, axis=0)
incidences_Uconst_97p5 = np.percentile(incidences_Uconst, 97.5, axis=0)
incidences_Uconst_mean = np.mean(incidences_Uconst, axis=0)

u_ad_Uconst_med = np.median(u_ad_Uconst, axis=0) # median and percentiles year by year
u_ad_Uconst_2p5 = np.percentile(u_ad_Uconst, 2.5, axis=0)
u_ad_Uconst_97p5 = np.percentile(u_ad_Uconst, 97.5, axis=0)
u_ad_Uconst_mean = np.mean(u_ad_Uconst, axis=0)

u_nad_Uconst_med = np.median(u_nad_Uconst, axis=0) # median and percentiles year by year
u_nad_Uconst_2p5 = np.percentile(u_nad_Uconst, 2.5, axis=0)
u_nad_Uconst_97p5 = np.percentile(u_nad_Uconst, 97.5, axis=0)
u_nad_Uconst_mean = np.mean(u_nad_Uconst, axis=0)

plt.plot(ages, incidences_Uconst_mean)
plt.plot(ages, incidences_Uconst_2p5, '--')
plt.plot(ages, incidences_Uconst_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Uconst_incid.png',dpi=300, bbox_inches = "tight")
plt.show()

age_table = ages[logicals]
prev_Uconst_med_table = prevalences_Uconst_med[logicals]
prev_Uconst_2p5_table = prevalences_Uconst_2p5[logicals]
prev_Uconst_97p5_table = prevalences_Uconst_97p5[logicals]
prev_Uconst_mean_table = prevalences_Uconst_mean[logicals]

incidences_Uconst_med_table = incidences_Uconst_med[logicals]
incidences_Uconst_2p5_table = incidences_Uconst_2p5[logicals]
incidences_Uconst_97p5_table = incidences_Uconst_97p5[logicals]
incidences_Uconst_mean_table = incidences_Uconst_mean[logicals]

u_ad_Uconst_med_table = u_ad_Uconst_med[logicals] 
u_ad_Uconst_2p5_table = u_ad_Uconst_2p5[logicals]
u_ad_Uconst_97p5_table = u_ad_Uconst_97p5[logicals]
u_ad_Uconst_mean_table = u_ad_Uconst_mean[logicals]

u_nad_Uconst_med_table = u_nad_Uconst_med[logicals] 
u_nad_Uconst_2p5_table = u_nad_Uconst_2p5[logicals]
u_nad_Uconst_97p5_table = u_nad_Uconst_97p5[logicals]
u_nad_Uconst_mean_table = u_nad_Uconst_mean[logicals]

print(indices_wanted)
print(age_table)
print("Prev")
print(prev_Uconst_med_table)
print(prev_Uconst_2p5_table)
print(prev_Uconst_97p5_table)
print(prev_Uconst_mean_table)
print("Incid")
print(incidences_Uconst_med_table)
print(incidences_Uconst_2p5_table)
print(incidences_Uconst_97p5_table)
print(incidences_Uconst_mean_table)
print("U AD+")
print(u_ad_Uconst_med_table) 
print(u_ad_Uconst_2p5_table)
print(u_ad_Uconst_97p5_table)
print(u_ad_Uconst_mean_table)
print("U AD-")
print(u_nad_Uconst_med_table) 
print(u_nad_Uconst_2p5_table)
print(u_nad_Uconst_97p5_table)
print(u_nad_Uconst_mean_table)


# In[16]:


# when U has a distribution but is fixed in time
incidences_Udist = np.zeros( (num_realizations, num_ages) )
prevalences_Udist = np.zeros( (num_realizations, num_ages) )

u_ad_Udist = np.zeros( (num_realizations, num_ages))
u_nad_Udist = np.zeros( (num_realizations, num_ages))

# now we consider the case where Ubar has a distribution but is constant over ages
for num in range(num_realizations): # per iteration
    
    print("num %d" % num)
    
    cohort = [Person( bin_dist() , lambda t: 1 ) for i in range(cohort_size)] # this many people in the cohort are healthy
    prev = np.array([0]) # prevalences
    incid = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad = np.mean( [p.getUnow(0) for p in cohort if (p.healthy and p.alive)])
    u_nad = np.mean( [p.getUnow(0) for p in cohort if ( (not p.healthy) and p.alive)] )
    
    prev_by_age = [0] # for each special age will store the stats for this simulation
    inc_by_age = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if (p.healthy and p.alive)]) ]
    u_nad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] ) ]
    
    
    for year in years_sub: # start at age t=0, go up to age year_end
        # look at each person
        for p in cohort:
            p.die(year) # remove those who die of AD
            
            if p.healthy: # need to simulate
                p.getAD(year*year_in_sec,dT_year_sim)        
        
        P_now = get_P(cohort)
        I_now = get_I(cohort, year*year_in_sec, dT_year_sim)
        
        u_ad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if (p.healthy and p.alive)])
        u_nad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] )
       
        prev = np.append(prev,P_now)
        incid = np.append(incid,I_now)
        u_ad = np.append(u_ad, u_ad_now)
        u_nad = np.append(u_nad, u_nad_now)
        
        res, yr = float_in(year, ages)       
        
        if res: # so a year to document            
            prev_by_age.append(P_now)
            inc_by_age.append(I_now)
            u_ad_by_age.append(u_ad_now)
            u_nad_by_age.append(u_nad_now)
    
    if random.uniform(0,1) < pr_plot:
        plt.subplot(1,2,1)
        plt.plot(years, 100*prev)
        plt.subplot(1,2,2)
        plt.plot(years, incid)
    
    incidences_Udist[num,:] = np.array([inc_by_age])
    prevalences_Udist[num,:] = np.array([prev_by_age])
    u_ad_Udist[num,:] = np.array([u_ad_by_age])
    u_nad_Udist[num,:] = np.array([u_nad_by_age])

plt.subplot(1,2,1)
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.subplot(1,2,2)
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Udist_many.png',dpi=300, bbox_inches = "tight")
plt.tight_layout()
plt.show()


# In[17]:


prevalences_Udist_med = np.median(prevalences_Udist, axis=0) # median and percentiles year by year
prevalences_Udist_2p5 = np.percentile(prevalences_Udist, 2.5, axis=0)
prevalences_Udist_97p5 = np.percentile(prevalences_Udist, 97.5, axis=0)
prevalences_Udist_mean = np.mean(prevalences_Udist, axis=0)

plt.plot(ages, 100*prevalences_Udist_mean)
plt.plot(ages, 100*prevalences_Udist_2p5, '--')
plt.plot(ages, 100*prevalences_Udist_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.savefig('Udist_prev.png',dpi=300, bbox_inches = "tight")
plt.show()

incidences_Udist_med = np.median(incidences_Udist, axis=0) # median and percentiles year by year
incidences_Udist_2p5 = np.percentile(incidences_Udist, 2.5, axis=0)
incidences_Udist_97p5 = np.percentile(incidences_Udist, 97.5, axis=0)
incidences_Udist_mean = np.mean(incidences_Udist, axis=0)

u_ad_Udist_med = np.median(u_ad_Udist, axis=0) # median and percentiles year by year
u_ad_Udist_2p5 = np.percentile(u_ad_Udist, 2.5, axis=0)
u_ad_Udist_97p5 = np.percentile(u_ad_Udist, 97.5, axis=0)
u_ad_Udist_mean = np.mean(u_ad_Udist, axis=0)

u_nad_Udist_med = np.median(u_nad_Udist, axis=0) # median and percentiles year by year
u_nad_Udist_2p5 = np.percentile(u_nad_Udist, 2.5, axis=0)
u_nad_Udist_97p5 = np.percentile(u_nad_Udist, 97.5, axis=0)
u_nad_Udist_mean = np.mean(u_nad_Udist, axis=0)

plt.plot(ages, incidences_Udist_mean)
plt.plot(ages, incidences_Udist_2p5, '--')
plt.plot(ages, incidences_Udist_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Udist_incid.png',dpi=300, bbox_inches = "tight")
plt.show()

age_table = ages[logicals]
prev_Udist_med_table = prevalences_Udist_med[logicals]
prev_Udist_2p5_table = prevalences_Udist_2p5[logicals]
prev_Udist_97p5_table = prevalences_Udist_97p5[logicals]
prev_Udist_mean_table = prevalences_Udist_mean[logicals]

incidences_Udist_med_table = incidences_Udist_med[logicals]
incidences_Udist_2p5_table = incidences_Udist_2p5[logicals]
incidences_Udist_97p5_table = incidences_Udist_97p5[logicals]
incidences_Udist_mean_table = incidences_Udist_mean[logicals]


u_ad_Udist_med_table = u_ad_Udist_med[logicals] 
u_ad_Udist_2p5_table = u_ad_Udist_2p5[logicals]
u_ad_Udist_97p5_table = u_ad_Udist_97p5[logicals]
u_ad_Udist_mean_table = u_ad_Udist_mean[logicals]

u_nad_Udist_med_table = u_nad_Udist_med[logicals] 
u_nad_Udist_2p5_table = u_nad_Udist_2p5[logicals]
u_nad_Udist_97p5_table = u_nad_Udist_97p5[logicals]
u_nad_Udist_mean_table = u_nad_Udist_mean[logicals]

print(indices_wanted)
print(age_table)
print("Prev")
print(prev_Udist_med_table)
print(prev_Udist_2p5_table)
print(prev_Udist_97p5_table)
print(prev_Udist_mean_table)
print("Incid")
print(incidences_Udist_med_table)
print(incidences_Udist_2p5_table)
print(incidences_Udist_97p5_table)
print(incidences_Udist_mean_table)
print("U AD+")
print(u_ad_Udist_med_table) 
print(u_ad_Udist_2p5_table)
print(u_ad_Udist_97p5_table)
print(u_ad_Udist_mean_table)
print("U AD-")
print(u_nad_Udist_med_table) 
print(u_nad_Udist_2p5_table)
print(u_nad_Udist_97p5_table)
print(u_nad_Udist_mean_table)


# In[18]:


# when U is fixed at t=0 but grows
incidences_Uconst_t = np.zeros( (num_realizations, num_ages) )
prevalences_Uconst_t = np.zeros( (num_realizations, num_ages) )

u_ad_Uconst_t = np.zeros( (num_realizations, num_ages))
u_nad_Uconst_t = np.zeros( (num_realizations, num_ages))

# now we consider the case where Ubar is fixed at t=0 but grows over ages
for num in range(num_realizations): # per iteration
    
    print("num %d" % num)
    
    cohort = [Person( Ubar , lambda t: rescale(t) ) for i in range(cohort_size)] # this many people in the cohort are healthy
    prev = np.array([0]) # prevalences
    incid = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad = np.mean( [p.getUnow(0) for p in cohort if (p.healthy and p.alive)])
    u_nad = np.mean( [p.getUnow(0) for p in cohort if ( (not p.healthy) and p.alive)] )
    
    prev_by_age = [0] # for each special age will store the stats for this simulation
    inc_by_age = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if (p.healthy and p.alive)]) ]
    u_nad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] ) ]
  
    for year in years_sub: # start at age t=0, go up to age year_end
        # look at each person
        for p in cohort:
            p.die(year) # remove those who die of AD
            
            if p.healthy: # need to simulate
                p.getAD(year*year_in_sec,dT_year_sim)        
        
        P_now = get_P(cohort)
        I_now = get_I(cohort, year*year_in_sec, dT_year_sim)
        
        u_ad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if (p.healthy and p.alive)])
        u_nad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] )
        
        prev = np.append(prev,P_now)
        incid = np.append(incid,I_now)
        u_ad = np.append(u_ad, u_ad_now)
        u_nad = np.append(u_nad, u_nad_now)
       
        res, yr = float_in(year, ages)       
        
        if res: # so a year to document            
            prev_by_age.append(P_now)
            inc_by_age.append(I_now)
            u_ad_by_age.append(u_ad_now)
            u_nad_by_age.append(u_nad_now)
    
    if random.uniform(0,1) < pr_plot:
        plt.subplot(1,2,1)
        plt.plot(years, 100*prev)
        plt.subplot(1,2,2)
        plt.plot(years, incid)
    
    incidences_Uconst_t[num,:] = np.array([inc_by_age])
    prevalences_Uconst_t[num,:] = np.array([prev_by_age])
    u_ad_Uconst_t[num,:] = np.array([u_ad_by_age])
    u_nad_Uconst_t[num,:] = np.array([u_nad_by_age])

plt.subplot(1,2,1)
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.subplot(1,2,2)
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Uconst_t_many.png',dpi=300, bbox_inches = "tight")
plt.tight_layout()
plt.show()


# In[19]:


prevalences_Uconst_t_med = np.median(prevalences_Uconst_t, axis=0) # median and percentiles year by year
prevalences_Uconst_t_2p5 = np.percentile(prevalences_Uconst_t, 2.5, axis=0)
prevalences_Uconst_t_97p5 = np.percentile(prevalences_Uconst_t, 97.5, axis=0)
prevalences_Uconst_t_mean = np.mean(prevalences_Uconst_t, axis=0)

plt.plot(ages, 100*prevalences_Uconst_t_mean)
plt.plot(ages, 100*prevalences_Uconst_t_2p5, '--')
plt.plot(ages, 100*prevalences_Uconst_t_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.savefig('Uconst_t_prev.png',dpi=300, bbox_inches = "tight")
plt.show()

incidences_Uconst_t_med = np.median(incidences_Uconst_t, axis=0) # median and percentiles year by year
incidences_Uconst_t_2p5 = np.percentile(incidences_Uconst_t, 2.5, axis=0)
incidences_Uconst_t_97p5 = np.percentile(incidences_Uconst_t, 97.5, axis=0)
incidences_Uconst_t_mean = np.mean(incidences_Uconst_t, axis=0)

u_ad_Uconst_t_med = np.median(u_ad_Uconst_t, axis=0) # median and percentiles year by year
u_ad_Uconst_t_2p5 = np.percentile(u_ad_Uconst_t, 2.5, axis=0)
u_ad_Uconst_t_97p5 = np.percentile(u_ad_Uconst_t, 97.5, axis=0)
u_ad_Uconst_t_mean = np.mean(u_ad_Uconst_t, axis=0)

u_nad_Uconst_t_med = np.median(u_nad_Uconst_t, axis=0) # median and percentiles year by year
u_nad_Uconst_t_2p5 = np.percentile(u_nad_Uconst_t, 2.5, axis=0)
u_nad_Uconst_t_97p5 = np.percentile(u_nad_Uconst_t, 97.5, axis=0)
u_nad_Uconst_t_mean = np.mean(u_nad_Uconst_t, axis=0)

plt.plot(ages, incidences_Uconst_t_mean)
plt.plot(ages, incidences_Uconst_t_2p5, '--')
plt.plot(ages, incidences_Uconst_t_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Uconst_t_incid.png',dpi=300, bbox_inches = "tight")
plt.show()

age_table = ages[logicals]
prev_Uconst_t_med_table = prevalences_Uconst_t_med[logicals]
prev_Uconst_t_2p5_table = prevalences_Uconst_t_2p5[logicals]
prev_Uconst_t_97p5_table = prevalences_Uconst_t_97p5[logicals]
prev_Uconst_t_mean_table = prevalences_Uconst_t_mean[logicals]

incidences_Uconst_t_med_table = incidences_Uconst_t_med[logicals]
incidences_Uconst_t_2p5_table = incidences_Uconst_t_2p5[logicals]
incidences_Uconst_t_97p5_table = incidences_Uconst_t_97p5[logicals]
incidences_Uconst_t_mean_table = incidences_Uconst_t_mean[logicals]

u_ad_Uconst_t_med_table = u_ad_Uconst_t_med[logicals] 
u_ad_Uconst_t_2p5_table = u_ad_Uconst_t_2p5[logicals]
u_ad_Uconst_t_97p5_table = u_ad_Uconst_t_97p5[logicals]
u_ad_Uconst_t_mean_table = u_ad_Uconst_t_mean[logicals]

u_nad_Uconst_t_med_table = u_nad_Uconst_t_med[logicals] 
u_nad_Uconst_t_2p5_table = u_nad_Uconst_t_2p5[logicals]
u_nad_Uconst_t_97p5_table = u_nad_Uconst_t_97p5[logicals]
u_nad_Uconst_t_mean_table = u_nad_Uconst_t_mean[logicals]

print(indices_wanted)
print(age_table)
print("Prev")
print(prev_Uconst_t_med_table)
print(prev_Uconst_t_2p5_table)
print(prev_Uconst_t_97p5_table)
print(prev_Uconst_t_mean_table)
print("Incid")
print(incidences_Uconst_t_med_table)
print(incidences_Uconst_t_2p5_table)
print(incidences_Uconst_t_97p5_table)
print(incidences_Uconst_t_mean_table)
print("U AD+")
print(u_ad_Uconst_t_med_table) 
print(u_ad_Uconst_t_2p5_table)
print(u_ad_Uconst_t_97p5_table)
print(u_ad_Uconst_t_mean_table)
print("U AD-")
print(u_nad_Uconst_t_med_table) 
print(u_nad_Uconst_t_2p5_table)
print(u_nad_Uconst_t_97p5_table)
print(u_nad_Uconst_t_mean_table)


# In[20]:


# when U is has a distribution at t=0 and grows
incidences_Udist_t = np.zeros( (num_realizations, num_ages) )
prevalences_Udist_t = np.zeros( (num_realizations, num_ages) )

u_ad_Udist_t = np.zeros( (num_realizations, num_ages))
u_nad_Udist_t = np.zeros( (num_realizations, num_ages))

# now we consider the case where Ubar is fixed at t=0 but grows over ages
for num in range(num_realizations): # per iteration
    
    print("num %d" % num)
    
    cohort = [Person( bin_dist() , lambda t: rescale(t) ) for i in range(cohort_size)] # this many people in the cohort are healthy
    prev = np.array([0]) # prevalences
    incid = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad = np.mean( [p.getUnow(0) for p in cohort if (p.healthy and p.alive)])
    u_nad = np.mean( [p.getUnow(0) for p in cohort if ( (not p.healthy) and p.alive)] )
    
    prev_by_age = [0] # for each special age will store the stats for this simulation
    inc_by_age = [ get_I(cohort, 0, dT_year_sim) ]
    
    u_ad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if (p.healthy and p.alive)]) ]
    u_nad_by_age = [ np.mean( [p.getUnow(0*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] ) ]
 
    for year in years_sub: # start at age t=0, go up to age year_end
        # look at each person
        for p in cohort:
            p.die(year) # remove those who die of AD
            
            if p.healthy: # need to simulate
                p.getAD(year*year_in_sec,dT_year_sim)        
        
        P_now = get_P(cohort)
        I_now = get_I(cohort, year*year_in_sec, dT_year_sim)
        
        u_ad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if (p.healthy and p.alive)])
        u_nad_now = np.mean( [p.getUnow(year*year_in_sec) for p in cohort if ( (not p.healthy) and p.alive)] )
                
        prev = np.append(prev,P_now)
        incid = np.append(incid,I_now)
        u_ad = np.append(u_ad, u_ad_now)
        u_nad = np.append(u_nad, u_nad_now)
        
        res, yr = float_in(year, ages)       
        
        if res: # so a year to document            
            prev_by_age.append(P_now)
            inc_by_age.append(I_now)
            u_ad_by_age.append(u_ad_now)
            u_nad_by_age.append(u_nad_now)
    
    if random.uniform(0,1) < pr_plot:
        plt.subplot(1,2,1)
        plt.plot(years, 100*prev)
        plt.subplot(1,2,2)
        plt.plot(years, incid)
    
    incidences_Udist_t[num,:] = np.array([inc_by_age])
    prevalences_Udist_t[num,:] = np.array([prev_by_age])
    u_ad_Udist_t[num,:] = np.array([u_ad_by_age])
    u_nad_Udist_t[num,:] = np.array([u_nad_by_age])

plt.subplot(1,2,1)
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.subplot(1,2,2)
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Udist_t_many.png',dpi=300, bbox_inches = "tight")
plt.tight_layout()
plt.show()


# In[21]:


prevalences_Udist_t_med = np.median(prevalences_Udist_t, axis=0) # median and percentiles year by year
prevalences_Udist_t_2p5 = np.percentile(prevalences_Udist_t, 2.5, axis=0)
prevalences_Udist_t_97p5 = np.percentile(prevalences_Udist_t, 97.5, axis=0)
prevalences_Udist_t_mean = np.mean(prevalences_Udist_t, axis=0)

plt.plot(ages, 100*prevalences_Udist_t_mean)
plt.plot(ages, 100*prevalences_Udist_t_2p5, '--')
plt.plot(ages, 100*prevalences_Udist_t_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.savefig('Udist_t_prev.png',dpi=300, bbox_inches = "tight")
plt.show()

incidences_Udist_t_med = np.median(incidences_Udist_t, axis=0) # median and percentiles year by year
incidences_Udist_t_2p5 = np.percentile(incidences_Udist_t, 2.5, axis=0)
incidences_Udist_t_97p5 = np.percentile(incidences_Udist_t, 97.5, axis=0)
incidences_Udist_t_mean = np.mean(incidences_Udist_t, axis=0)

u_ad_Udist_t_med = np.median(u_ad_Udist_t, axis=0) # median and percentiles year by year
u_ad_Udist_t_2p5 = np.percentile(u_ad_Udist_t, 2.5, axis=0)
u_ad_Udist_t_97p5 = np.percentile(u_ad_Udist_t, 97.5, axis=0)
u_ad_Udist_t_mean = np.mean(u_ad_Udist_t, axis=0)

u_nad_Udist_t_med = np.median(u_nad_Udist_t, axis=0) # median and percentiles year by year
u_nad_Udist_t_2p5 = np.percentile(u_nad_Udist_t, 2.5, axis=0)
u_nad_Udist_t_97p5 = np.percentile(u_nad_Udist_t, 97.5, axis=0)
u_nad_Udist_t_mean = np.mean(u_nad_Udist_t, axis=0)

plt.plot(ages, incidences_Udist_t_mean)
plt.plot(ages, incidences_Udist_t_2p5, '--')
plt.plot(ages, incidences_Udist_t_97p5, '--')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.savefig('Udist_t_incid.png',dpi=300, bbox_inches = "tight")
plt.show()

age_table = ages[logicals]
prev_Udist_t_med_table = prevalences_Udist_t_med[logicals]
prev_Udist_t_2p5_table = prevalences_Udist_t_2p5[logicals]
prev_Udist_t_97p5_table = prevalences_Udist_t_97p5[logicals]
prev_Udist_t_mean_table = prevalences_Udist_t_mean[logicals]

incidences_Udist_t_med_table = incidences_Udist_t_med[logicals]
incidences_Udist_t_2p5_table = incidences_Udist_t_2p5[logicals]
incidences_Udist_t_97p5_table = incidences_Udist_t_97p5[logicals]
incidences_Udist_t_mean_table = incidences_Udist_t_mean[logicals]

u_ad_Udist_t_med_table = u_ad_Udist_t_med[logicals] 
u_ad_Udist_t_2p5_table = u_ad_Udist_t_2p5[logicals]
u_ad_Udist_t_97p5_table = u_ad_Udist_t_97p5[logicals]
u_ad_Udist_t_mean_table = u_ad_Udist_t_mean[logicals]

u_nad_Udist_t_med_table = u_nad_Udist_t_med[logicals] 
u_nad_Udist_t_2p5_table = u_nad_Udist_t_2p5[logicals]
u_nad_Udist_t_97p5_table = u_nad_Udist_t_97p5[logicals]
u_nad_Udist_t_mean_table = u_nad_Udist_t_mean[logicals]

print(indices_wanted)
print(age_table)
print("Prev")
print(prev_Udist_t_med_table)
print(prev_Udist_t_2p5_table)
print(prev_Udist_t_97p5_table)
print(prev_Udist_t_mean_table)
print("Incid")
print(incidences_Udist_t_med_table)
print(incidences_Udist_t_2p5_table)
print(incidences_Udist_t_97p5_table)
print(incidences_Udist_t_mean_table)
print("U AD+")
print(u_ad_Udist_t_med_table) 
print(u_ad_Udist_t_2p5_table)
print(u_ad_Udist_t_97p5_table)
print(u_ad_Udist_t_mean_table)
print("U AD-")
print(u_nad_Udist_t_med_table) 
print(u_nad_Udist_t_2p5_table)
print(u_nad_Udist_t_97p5_table)
print(u_nad_Udist_t_mean_table)


# In[22]:


"""Compare analytic approximation and stochastic averages to each other in the hypothetical scenario"""

# all these functions should take t in years!
def Prev_Uconst(t):
    if(t<T_D):
        return 1 - np.exp(-gamma*Ubar*t*year_in_sec)
    else:
        return 1 - np.exp(-gamma*Ubar*T_D*year_in_sec)
    
def Incid_Uconst(t):
    return gamma*Ubar

plt.plot(ages, 100*prevalences_Uconst_mean,'--',label='mean')
plt.plot(ages, [100*Prev_Uconst(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.legend()
plt.savefig('Uconst_prev_comp.png',dpi=300, bbox_inches = "tight")
plt.show()

plt.plot(ages, incidences_Uconst_mean,'--',label='mean')
plt.plot(ages, [year_in_sec*Incid_Uconst(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence [yr$^{-1}$]')
plt.legend()
plt.savefig('Uconst_incid_comp.png',dpi=300, bbox_inches = "tight")
plt.show()

def Prev_Udist(t):
    if(t<T_D):
        bot_P_Udist = lambda t: 1 
    else:
        bot_P_Udist = lambda t: pUmin*np.exp(-gamma*Umin*(t-T_D)*year_in_sec) + (1-pUmin)*np.exp(-gamma*Uplus*(t-T_D)*year_in_sec)   
        
    if(t<T_D):
        top_P_Udist = lambda t: pUmin*(1-np.exp(-gamma*Umin*t*year_in_sec)) + (1-pUmin)*(1-np.exp(-gamma*Uplus*t*year_in_sec))
    else:
        top_P_Udist = lambda t: pUmin*np.exp(-gamma*Umin*(t-T_D)*year_in_sec)*(1-np.exp(-gamma*Umin*(T_D)*year_in_sec)) + (1-pUmin)*np.exp(-gamma*Uplus*(t-T_D)*year_in_sec)*(1-np.exp(-gamma*Uplus*(T_D)*year_in_sec))
    
    return top_P_Udist(t)/bot_P_Udist(t)
    
def Incid_Udist(t):
    if(t<T_D):
        bot_I_Udist = lambda t: pUmin*np.exp(-gamma*Umin*t*year_in_sec) + (1-pUmin)*np.exp(-gamma*Uplus*t*year_in_sec)
    else:
        bot_I_Udist = lambda t: pUmin*np.exp(-gamma*Umin*T_D*year_in_sec) + (1-pUmin)*np.exp(-gamma*Uplus*T_D*year_in_sec)
        
    top_I_Udist = lambda t: gamma*(pUmin*Umin*np.exp(-gamma*Umin*t*year_in_sec) + (1-pUmin)*Uplus*np.exp(-gamma*Uplus*t*year_in_sec))
    
    return top_I_Udist(t)/bot_I_Udist(t)

plt.plot(ages, 100*prevalences_Udist_mean,'--',label='mean')
plt.plot(ages, [100*Prev_Udist(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.legend()
plt.savefig('Udist_prev_comp.png',dpi=300, bbox_inches = "tight")
plt.show()

plt.plot(ages, incidences_Udist_mean,'--',label='mean')
plt.plot(ages, [year_in_sec*Incid_Udist(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence [yr$^{-1}$]')
plt.legend()
plt.savefig('Udist_incid_comp.png',dpi=300, bbox_inches = "tight")
plt.show()

def Xi_t(t):
    return (1+t/tau_s)**2/(1-t/tau_k)**2

def I_0_t_Xi(t):
    aloc = 1/tau_s
    bloc = 1/tau_k
    return (aloc**2*bloc*t + (aloc+bloc)**2/(1-bloc*t) + 2*aloc*(aloc+bloc)*np.log(1-bloc*t)-(aloc+bloc)**2)/bloc**3
    
    #return tau_k**3*( (t/tau_k*(1/tau_s**2*(t/tau_k-2)) - 2/(tau_s*tau_k) - 1/tau_k**2)/(t/tau_k-1) - 2/tau_s*(-1/tau_s-1/tau_k)*np.log(1-t/tau_k))

def I_t1_t2_Xi(t1,t2):
    return I_0_t_Xi(t2)-I_0_t_Xi(t1)

def Prev_Uconst_t(t):
    if(t<T_D):
        return 1 - np.exp(-gamma*Ubar*I_t1_t2_Xi(0,t*year_in_sec))
    else:
        return 1 - np.exp(-gamma*Ubar*( I_t1_t2_Xi( (t-T_D)*year_in_sec,t*year_in_sec) ))
    
def Incid_Uconst_t(t):
    return gamma*Ubar*Xi_t(t*year_in_sec)

plt.plot(ages, 100*prevalences_Uconst_t_mean,'--',label='mean')
plt.plot(ages, [100*Prev_Uconst_t(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.legend()
plt.savefig('Uconst_t_prev_comp.png',dpi=300, bbox_inches = "tight")
plt.show()

plt.plot(ages, incidences_Uconst_t_mean,'--',label='mean')
plt.plot(ages, [year_in_sec*Incid_Uconst_t(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence [yr$^{-1}$]')
plt.legend()
plt.savefig('Uconst_t_incid_comp.png',dpi=300, bbox_inches = "tight")
plt.show()


def Prev_Udist_t(t):
    if(t<T_D):
        bot_P_Udist_t = lambda t: 
    else:
        bot_P_Udist_t = lambda t: pUmin*np.exp(-gamma*Umin*I_0_t_Xi( (t-T_D)*year_in_sec)) + (1-pUmin)*np.exp(-gamma*Uplus*I_0_t_Xi((t-T_D)*year_in_sec))   
        
    if(t<T_D):
        top_P_Udist_t = lambda t: pUmin*(1-np.exp(-gamma*Umin*I_0_t_Xi(t*year_in_sec) ) ) + (1-pUmin)*(1-np.exp(-gamma*Uplus*I_0_t_Xi(t*year_in_sec) ) )
    else:
        top_P_Udist_t = lambda t: pUmin*np.exp(-gamma*Umin* I_0_t_Xi((t-T_D)*year_in_sec) )*(1-np.exp(-gamma*Umin*I_t1_t2_Xi((t-T_D)*year_in_sec, t*year_in_sec))) + (1-pUmin)*np.exp(-gamma*Uplus* I_0_t_Xi( (t-T_D)*year_in_sec) )*(1-np.exp(-gamma*Uplus*I_t1_t2_Xi((t-T_D)*year_in_sec, t*year_in_sec)))    
    
    return top_P_Udist_t(t) / bot_P_Udist_t(t) #/bot_P_Udist_t(t)
    
def Incid_Udist_t(t):
    top_I_Udist_t = lambda t: gamma*(pUmin*Umin*Xi_t(t*year_in_sec))*np.exp(-gamma*Umin*I_0_t_Xi(t*year_in_sec)) + (1-pUmin)*Uplus*Xi_t(t*year_in_sec)**np.exp(-gamma*Uplus*I_0_t_Xi(t*year_in_sec))
    bot_I_Udist_t = lambda t: pUmin*np.exp(-gamma*Umin*I_0_t_Xi(t*year_in_sec)) + (1-pUmin)*np.exp(-gamma*Uplus*I_0_t_Xi(t*year_in_sec))
    return top_I_Udist_t(t)/bot_I_Udist_t(t)
                
    
plt.plot(ages, 100*prevalences_Udist_t_mean,'--',label='mean')
plt.plot(ages, [100*Prev_Udist_t(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.legend()
plt.savefig('Udist_t_prev_comp.png',dpi=300, bbox_inches = "tight")
plt.show()

plt.plot(ages, incidences_Udist_t_mean,'--',label='mean')
plt.plot(ages, [year_in_sec*Incid_Udist_t(a) for a in ages],label='model')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence [yr$^{-1}$]')
plt.legend()
plt.savefig('Udist_t_incid_comp.png',dpi=300, bbox_inches = "tight")
plt.show()


# In[23]:


"""Sensitivity Analysis at age 65"""
T65 = 65

om0 = 0.5*Sbar**2*nubar*sigmabar/(mubar*kappabar**2)

dPdom = I_t1_t2_Xi((T65-T_D)*year_in_sec, T65*year_in_sec)*np.exp(-om0*I_t1_t2_Xi((T65-T_D)*year_in_sec, T65*year_in_sec))
P0 = 1 - np.exp(-om0*I_t1_t2_Xi((T65-T_D)*year_in_sec, T65*year_in_sec))
sensP_om = om0/P0*dPdom

dIdom = Xi_t(T65*year_in_sec)
I0 = 1 - np.exp(-om0*I_t1_t2_Xi((T65-T_D)*year_in_sec, T65*year_in_sec))
sensP_om = om0/P0*dPdom

print(sensP_om)

def Prev_Uconst_t_om(om,t):
    if(t<T_D):
        return 1 - np.exp(-om*I_t1_t2_Xi(0,t*year_in_sec))
    else:
        return 1 - np.exp(-om*( I_t1_t2_Xi( (t-T_D)*year_in_sec,t*year_in_sec) ))
    
def Incid_Uconst_t_om(om,t):
    return om*Xi_t(t*year_in_sec)

ages_100 = [t for t in range(101)]

plt.plot(ages_100, [100*Prev_Uconst_t_om(om0/4,a) for a in ages_100],linewidth='1', label=r'${\omega_0\leftarrow\omega_0/4}$')
plt.plot(ages_100, [100*Prev_Uconst_t_om(om0/2,a) for a in ages_100],linewidth='2',label=r'${\omega_0\leftarrow\omega_0/2}$')
plt.plot(ages_100, [100*Prev_Uconst_t_om(om0/1,a) for a in ages_100],linewidth='3',label=r'${\omega_0\leftarrow\omega_0}$')
plt.plot(ages_100, [100*Prev_Uconst_t_om(2*om0,a) for a in ages_100],linewidth='4',label=r'${\omega_0\leftarrow 2\omega_0}$')
plt.plot(ages_100, [100*Prev_Uconst_t_om(4*om0,a) for a in ages_100],linewidth='5',label=r'${\omega_0\leftarrow 4\omega}$')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.legend()
plt.savefig('Prev_Vary_omega.png',dpi=300, bbox_inches = "tight")
plt.show()

plt.plot(ages_100, [year_in_sec*Incid_Uconst_t_om(om0/4,a) for a in ages_100],linewidth='1',label=r'${\omega_0\leftarrow\omega_0/4}$')
plt.plot(ages_100, [year_in_sec*Incid_Uconst_t_om(om0/2,a) for a in ages_100],linewidth='2',label=r'${\omega_0\leftarrow\omega_0/2}$')
plt.plot(ages_100, [year_in_sec*Incid_Uconst_t_om(om0/1,a) for a in ages_100],linewidth='3',label=r'${\omega_0\leftarrow\omega_0}$')
plt.plot(ages_100, [year_in_sec*Incid_Uconst_t_om(2*om0,a) for a in ages_100],linewidth='4',label=r'${\omega_0\leftarrow 2\omega_0}$')
plt.plot(ages_100, [year_in_sec*Incid_Uconst_t_om(4*om0,a) for a in ages_100],linewidth='5',label=r'${\omega_0\leftarrow 4\omega}$')
plt.xlabel("Age [yr]")
plt.ylabel(r'Incidence [yr$^{-1}$]')
plt.legend()
#plt.yscale('log')
plt.savefig('Incid_Vary_omega.png',dpi=300, bbox_inches = "tight")
plt.show()

print(Prev_Uconst_t_om(100*om0,85))

print(1.96*np.sqrt( (1 - np.exp(-gamma*Ubar*T_D*year_in_sec))*np.exp(-gamma*Ubar*T_D*year_in_sec) / cohort_size ) )


# In[24]:


"""Looking at data on prevalence vs age for men and women"""

ad_prev_ages = [50, 55, 58, 60, 62, 65, 70, 75, 80, 85,90,95,100]
ad_prev_male = [0, 0.56, 0.74, 0.93, 1.30, 1.48, 4.07, 5.09, 9.17, 13.89, 20.37, 22.87, 25.37]
ad_prev_female = [0, 0.56, 0.74, 0.93, 1.30, 1.48, 3.33, 5.83, 10.74, 17.96, 25.93, 32.59, 34.26]

plt.plot(ad_prev_ages, ad_prev_male, linewidth='5', label='M')
plt.plot(ad_prev_ages, ad_prev_female, linewidth='3', label='F')
plt.xlabel("Age [yr]")
plt.ylabel("Prevalence [%]")
plt.legend()
plt.savefig('Prev_MF.png',dpi=300, bbox_inches = "tight")
plt.show()


# In[33]:


"""Comparions of stochastic and approximate U_0 values conditioned on disease vs age"""

plt.plot(ages, u_ad_Udist_mean, 'r-', linewidth = '2', label= r'AD$^-$')
plt.plot(ages, u_nad_Udist_mean, 'r-.', linewidth = '2', label= r'AD$^+$')
plt.plot(ages, [Ubar for a in ages], 'g--', linewidth='2', label= r'$\bar U$')
plt.plot(ages, [(1+Frat**2)*Ubar for a in ages], 'g:', linewidth='2', label= r'$(1+{F^*}^2) \bar U$')
plt.xlabel('Age [yr]')
plt.ylabel(r'$U_0$ [s$^{-1}$]')
plt.legend()
plt.savefig('Static_U_AD.png',dpi=300, bbox_inches = "tight")
plt.show()

plt.plot(ages, [u_ad_Udist_t_mean[i]/Xi_t(ages[i]*year_in_sec) for i in range(len(ages))], 'r-', linewidth = '2', label=r'AD$^-$')
plt.plot(ages, [u_nad_Udist_t_mean[i]/Xi_t(ages[i]*year_in_sec) for i in range(len(ages))], 'r-.', linewidth = '2', label= r'AD$^+$')
plt.plot(ages, [Ubar for a in ages], 'g--', linewidth='2', label= r'$\bar U$')
plt.plot(ages, [(1+Frat**2)*Ubar for a in ages], 'g:', linewidth='2', label= r'$(1+{F^*}^2) \bar U$')
plt.xlabel('Age [yr]')
plt.ylabel(r'$U_0$ [s$^{-1}$]')
plt.legend()
plt.savefig('Dynamic_U_AD.png',dpi=300, bbox_inches = "tight")
plt.show()


# In[26]:


'''This is purely speculative
Test what happens if sigma = (1+t/lambda_sigma)'''

# integral (1+cx)(1+ax)^2/(1+bx)^2
def I_a_b_c(a,b,c,x):
    return 1/(2*b**4)*(a**2*b**2*c*x**2 - 2*(a+b)**2*(-b-c)/(-b*x+1) - 2*a*b*x*(a*(-b-2*c)-2*b*c) - 2*(a+b)*(-2*a*b-3*a*c-b*c)*np.log(-b*x+1))

def I_sig_kap_S_t1_t2(tau_sig,t1,t2):
    return I_a_b_c(1/tau_s,1/tau_k,1/tau_sig,t2) - I_a_b_c(1/tau_s,1/tau_k,1/tau_sig,t1)

# want a doubling time of 4.9 yr ==>
Tscale_double = 4.9/np.log(2)

# want ~ 1/Tscale_double = 1/tau_sig + 2/tau_k + 2/tau_s ==>
tsig_test_yr = 1/ ( 1/Tscale_double - 2/(tau_k/year_in_sec) - 2/(tau_s/year_in_sec)  )
print(tsig_test_yr) # 9.01 yr

tsig_test = tsig_test_yr * year_in_sec

# still want HVol consistency...
# age age 75, -0.336% = V'/V
# V = exp(-Ubar integral Xi)
# 0.336e-2 = Ubar Xi
print(Ubar * Xi_t(75*year_in_sec)*year_in_sec)

Xi_t_new = lambda t: (1+t/tsig_test)*(1+t/tau_s)**2/(1-t/tau_k)**2
print(Ubar * Xi_t_new(75*year_in_sec)*year_in_sec)

# the ratio to rescale sigma...
sigma_ratio = Xi_t(75*year_in_sec)/Xi_t_new(75*year_in_sec)
print(sigma_ratio)

gamma_new = incidence_rates[0]/(Ubar*sigma_ratio*Xi_t_new(60*year_in_sec)*year_in_sec)
print(gamma_new)


def Prev_sig(tau_sig,t):
    return 1 - np.exp(-gamma_new*Ubar*sigma_ratio*I_sig_kap_S_t1_t2(tsig_test,year_in_sec*(t-T_D), year_in_sec*t))

def Incid_sig(t):
    return gamma_new*Ubar*sigma_ratio*Xi_t_new(t)

plt.plot(ages, [Prev_sig(tsig_test,a) for a in ages])
plt.show()



# prevalence functions can accept argument in years
plt.plot(prevalence_ages, prevalence_rates, 'g*', markersize='12', label='Clinical')
plt.plot(prevalence_range, Prev_sig(tsig_test, prevalence_range), 'k^', markersize='12', label='Dynamic')
plt.plot(prevalence_range, np.exp( ( poly( prev_fit  ) )(prevalence_range) ), 'g.' ) 
plt.plot(prevalence_range, np.exp( ( poly( prev_fit_model ) )(prevalence_range) ), 'k-' )
plt.xlabel('Age [yr]')
plt.ylabel('Prevalence')
plt.semilogy()
plt.legend()
plt.savefig('Sigma_t_Prevalence.png',dpi=300, bbox_inches = "tight")
plt.show()

# incidence functions accept argument in seconds
plt.plot(incidence_ages, incidence_rates, 'g*', markersize='12', label='Clinical')
plt.plot(incidence_range, Incid_sig(year_to_sec(incidence_range))*year_in_sec, 'k^', markersize='12', label='Dynamic')
plt.plot(incidence_range, np.exp( ( poly( incid_fit  ) )(incidence_range) ), 'g.' ) 
plt.plot(incidence_range, np.exp( ( poly( incid_fit_model ) )(incidence_range) ), 'k-' ) 
plt.xlabel('Age [yr]')
plt.ylabel(r'Incidence Rate [yr$^{-1}$]')
plt.semilogy()
plt.legend()
plt.savefig('Sigma_t_Incidence.png',dpi=300, bbox_inches = "tight")
plt.show()

prev_fit_model_sigma_t = np.polyfit( prevalence_range, np.log( Prev_sig(tsig_test, prevalence_range)), 1)
incid_fit_model_sigma_t = np.polyfit( incidence_range, np.log( Incid_sig(year_to_sec(incidence_range))*year ), 1)

double_time_prevalence_model_sigma_t = np.log(2)/prev_fit_model_sigma_t[0]
double_time_incidence_model_sigma_t = np.log(2)/incid_fit_model_sigma_t[0]


print(double_time_incidence_model_sigma_t)
print(double_time_prevalence_model_sigma_t)


plt.plot(age_range, np.exp(-Ubar*sigma_ratio*I_sig_kap_S_t1_t2(tsig_test,0,age_range*year_in_sec)), 'bv', markersize='10', label='Dynamic')
plt.xlabel('Age [yr]')
plt.ylabel('Fractional HV Remaining')
plt.legend()
plt.savefig('Sigma_t_HVOL.png',dpi=300, bbox_inches = "tight")
plt.show()